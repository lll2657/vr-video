﻿using UnityEngine;
using GVRSample;

public class newVideoPlayerReference : MonoBehaviour {

	public GvrVideoPlayerTexture player;

	void Awake() {
		#if !UNITY_5_2
		GetComponentInChildren<newVideoControls>(true).Player = player;
		#else
		GetComponentInChildren<newVideoControls>().Player = player;
		#endif
	}
}
