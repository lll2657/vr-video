﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

public class ChooseProject : MonoBehaviour {

	private JSONObject data;

	public GameObject chooseBtn;
	public GameObject buttonParent;

	IEnumerator Start () {
		VRSettings.enabled = false;
		WWW www = new WWW (Constants.Host + "/acquire.php");
		yield return www;
		data = new JSONObject (www.text);
		for (int i = 0; i < data.Count; i++) {
			Constants.receivedJson = JsonUtility.FromJson<Serialization.ReceivedJSON> (data[i].ToString());
			GameObject newChooseBtn = (GameObject)Instantiate (chooseBtn, buttonParent.transform);
			newChooseBtn.transform.localScale = Vector3.one;
			newChooseBtn.transform.GetChild(0).transform.GetComponent<Text>().text = Constants.receivedJson.name;
			newChooseBtn.name = i.ToString();
			newChooseBtn.SetActive (true);
		}
	}

	public void chose(GameObject button){
		Constants.receivedJson = JsonUtility.FromJson<Serialization.ReceivedJSON> (data[int.Parse(button.name)].ToString());
		SceneManager.LoadScene ("MyTry", LoadSceneMode.Single);
		VRSettings.enabled = true;
	}
}
