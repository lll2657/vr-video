﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Serialization : MonoBehaviour {


	#region Json Classes

	[Serializable]
	public class EntireInputJSON
	{
		public List<InputJSON> input;
	}

	[Serializable]
	public class InputJSON
	{
		public long time;
		public string name;
		public string type;
		public float posX;
		public float posY;
		public float posZ;
		public float rotX;
		public float rotY;
		public float rotZ;
		public string file;
		public string text;
		public string question;
		public string answer1;
		public string answer2;
		public string answer3;
	}

	public class ReceivedJSON
	{
		public int id;
		public string name;
		public string video;
		public string data;
		public string create_id;
	}

	#endregion
}
