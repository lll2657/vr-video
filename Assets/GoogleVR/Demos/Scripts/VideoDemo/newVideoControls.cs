﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class newVideoControls : MonoBehaviour {

	public Serialization.EntireInputJSON gameData;

	public GameObject image;
	public GameObject text;
	public GameObject questionObj;
	public Text question;
	public Button answer1;
	public Button answer2;
	public Button answer3;
	public GameObject button;
	public GameObject canvas;
	public GameObject playBtn;

	private int index;

	public GvrVideoPlayerTexture Player
	{
		set;
		get;
	}

	void Start() {
		index = 0;
		Player.videoURL = Constants.receivedJson.video;
		Constants.receivedJson.data = Constants.receivedJson.data.Replace("'","\"");
		gameData = JsonUtility.FromJson<Serialization.EntireInputJSON> (Constants.receivedJson.data);
		print ("THIS: "+gameData.input [index].type);
		if (Player != null) {
			Player.Init();
		}
		playBtn.SetActive (true);
		Player.Play ();
	}

	void Update() {
		if (Player.CurrentPosition > 0) {
			playBtn.SetActive (false);
		}
		if (index < gameData.input.Count) {
			if (gameData.input [index].time < Player.CurrentPosition) {
				if (gameData.input [index].type == "Image") {
					StartCoroutine (loadImage ());
					++index;
				} else if (gameData.input [index].type == "Text") {
					GameObject btnObj = Instantiate (button, new Vector3 (gameData.input [index].posX * Constants.scale, 
						gameData.input [index].posY * Constants.scale, gameData.input [index].posZ * Constants.scale), 
						                    Quaternion.Euler (gameData.input [index].rotX, gameData.input [index].rotY,
							                    gameData.input [index].rotZ));
					btnObj.name = gameData.input [index].name;
					btnObj.transform.SetParent (canvas.transform, false);
					btnObj.SetActive (true);
					GameObject textObj = Instantiate (text, new Vector3 (gameData.input [index].posX * Constants.scale, 
						gameData.input [index].posY * Constants.scale, gameData.input [index].posZ * Constants.scale), 
						                     Quaternion.Euler (gameData.input [index].rotX, gameData.input [index].rotY,
							                     gameData.input [index].rotZ));
					textObj.name = gameData.input [index].name + "text";
					textObj.transform.GetComponent<Text> ().text = gameData.input [index].text;
					textObj.transform.SetParent (canvas.transform, false);
					++index;
				} else if (gameData.input [index].type == "Question") {
					Player.Pause ();
					questionObj.transform.position = new Vector3 (gameData.input [index].posX * Constants.scale, 
						gameData.input [index].posY * Constants.scale, gameData.input [index].posZ * Constants.scale);
					questionObj.transform.rotation = Quaternion.Euler (gameData.input [index].rotX, 
						gameData.input [index].rotY, gameData.input [index].rotZ);
					question.text = gameData.input [index].question;
					answer1.GetComponentsInChildren<Text> () [0].text = gameData.input [index].answer1;
					answer2.GetComponentsInChildren<Text> () [0].text = gameData.input [index].answer2;
					answer3.GetComponentsInChildren<Text> () [0].text = gameData.input [index].answer3;
					answer1.transform.SetSiblingIndex (Random.Range (1, 4));
					answer2.transform.SetSiblingIndex (Random.Range (1, 4));
					answer3.transform.SetSiblingIndex (Random.Range (1, 4));
					questionObj.SetActive (true);
					++index;
				} else if (gameData.input [index].type == "Destroy") {
					Destroy (canvas.transform.Find (gameData.input [index].name).gameObject);
					if (canvas.transform.Find (gameData.input [index].name + "text") != null) {
						Destroy (canvas.transform.Find (gameData.input [index].name + "text").gameObject);
					}
					++index;
				}
			}
		}
	}

	public void OnPlayPause() {
		Player.Play ();
	}

	public void answered() {
		StartCoroutine (wait ());
	}

	public void clicked(GameObject btn) {
		btn.SetActive (false);
		canvas.transform.Find (btn.name + "text").gameObject.SetActive (true);
	}

	IEnumerator wait() {
		yield return new WaitForSeconds (1);
		answer1.interactable = true;
		answer2.interactable = true;
		answer3.interactable = true;
		questionObj.SetActive (false);
		Player.Play ();
	}

	IEnumerator loadImage(){
		GameObject imgObj = Instantiate (image, new Vector3 (gameData.input [index].posX * Constants.scale, 
			gameData.input [index].posY * Constants.scale, gameData.input [index].posZ * Constants.scale), 
			Quaternion.Euler (gameData.input [index].rotX, gameData.input [index].rotY,
				gameData.input [index].rotZ));
		imgObj.name = gameData.input [index].name;
		imgObj.transform.SetParent (canvas.transform, false);
		Texture2D tex;
		tex = new Texture2D (100, 100, TextureFormat.RGB24, false);
		WWW www = new WWW (Constants.Host + "/get_images.php?create_id='"+Constants.receivedJson.create_id + 
			"'&name='"+gameData.input [index].file+"'");
		yield return www;
		tex.LoadImage(www.bytes);
		Rect rec = new Rect(0, 0, tex.width, tex.height);
		Sprite newSprite = Sprite.Create (tex, rec, new Vector2 (0.5f, 0.5f));
		imgObj.transform.GetComponent<Image> ().sprite = newSprite;
		imgObj.SetActive (true);
	}
}
