﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Networking;

public class editorVideoControlsManager : MonoBehaviour {

	public Text projectName;
	public Text videoURL;

	public GameObject newProject;
	public GameObject videoControls;

	private GameObject pauseSprite;
	private GameObject playSprite;

	public Button playBtn;
	public GameObject menu;
	public Camera m_camera;

	private Slider videoScrubber;
	private Vector3 basePosition;
	private Text videoPosition;
	private Text videoDuration;
	private int index;

	public GameObject canvas;
	public GameObject inputField;
	public InputField inputFieldObj;
	public GameObject textObj;

	public GameObject filePath;
	public InputField filePathObj;
	public GameObject imgObj;

	public GameObject questionEditor;
	public InputField questionInput;
	public InputField answer1Input;
	public InputField answer2Input;
	public InputField answer3Input;
	public GameObject questionObj;
	public Text question;
	public Button answer1;
	public Button answer2;
	public Button answer3;

	public Serialization.EntireInputJSON gameData;
	public List<Serialization.InputJSON> gameDataList;

	public VideoPlayer Player
	{
		set;
		get;
	}

	void Awake() {
		foreach (Text t in GetComponentsInChildren<Text>()) {
			if (t.gameObject.name == "curpos_text") {
				videoPosition = t;
			} else if (t.gameObject.name == "duration_text") {
				videoDuration = t;
			}
		}

		foreach (RawImage raw in GetComponentsInChildren<RawImage>(true)) {
			if (raw.gameObject.name == "playImage") {
				playSprite = raw.gameObject;
			} else if (raw.gameObject.name == "pauseImage") {
				pauseSprite = raw.gameObject;
			}
		}

		foreach (Slider s in GetComponentsInChildren<Slider>(true)) {
			if (s.gameObject.name == "video_slider") {
				videoScrubber = s;
				videoScrubber.maxValue = 100;
				videoScrubber.minValue = 0;
			}
		}
	}

	void Update() {
		if (Input.GetKey (KeyCode.N)) {
			if (Input.GetMouseButtonDown (0)) {
				Player.Pause ();
				menu.transform.position = m_camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 3));
				menu.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
				menu.SetActive (true);
			}
		}
		if ((!Player.isPrepared || !Player.isPlaying)) {
			pauseSprite.SetActive(false);
			playSprite.SetActive(true);
		} else if (Player.isPrepared && Player.isPlaying) {
			pauseSprite.SetActive(true);
			playSprite.SetActive(false);
		}

		if (Player.isPrepared) {
			if (basePosition == Vector3.zero) {
				basePosition = videoScrubber.handleRect.localPosition;
			}
			videoScrubber.maxValue = (float)Player.clip.length*1000;
			videoScrubber.value = (float)Player.time*1000;
		
			videoPosition.text = FormatTime((long)Player.time*1000);
			videoDuration.text = FormatTime((long)Player.clip.length*1000);

		} else {
			videoScrubber.value = 0;
		}
	}

	void OnApplicationQuit() {
		StartCoroutine(Upload());
		File.WriteAllText(Application.dataPath + "/GoogleVR/Resources/Data/data.json", JsonUtility.ToJson(gameData));
	}

	public void createNew() {
		Player.url = videoURL.text;
		if (Player != null) {
			Player.Prepare();
		}
		index = 0;
		gameData = new Serialization.EntireInputJSON ();
		gameDataList = new List<Serialization.InputJSON> ();
		newProject.SetActive (false);
		videoControls.transform.position = new Vector3 (0,0,0);
	}

	public void OnPlayPause() {
		bool isPaused = !(Player.isPlaying);
		if (isPaused) {
			Player.Play();
		} else {
			Player.Pause();
		}
		pauseSprite.SetActive(isPaused);
		playSprite.SetActive(!isPaused);
	}

	public void skip() {
		Player.time = Player.time + 1;
	}

	public void back() {
		Player.time = Player.time - 1;
	}

	public void input() {
		inputField.transform.position = menu.transform.position;
		inputField.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		inputField.SetActive (true);
		menu.SetActive (false);
	}

	public void text() {
		inputField.SetActive (false);
		GameObject newTextObj = Instantiate (textObj, canvas.transform);
		newTextObj.name = "text_"+index;
		newTextObj.transform.position = menu.transform.position;
		newTextObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		newTextObj.transform.GetComponent<Text> ().text = inputFieldObj.text;
		inputFieldObj.text = "";
		newTextObj.transform.GetChild (0).name = newTextObj.name;
		newTextObj.SetActive (true);
		Serialization.InputJSON newText = new Serialization.InputJSON ();
		newText.time = (long) (Player.time * 1000);
		newText.name = newTextObj.name;
		newText.type = "Text";
		newText.posX = newTextObj.transform.position.x;
		newText.posY = newTextObj.transform.position.y;
		newText.posZ = newTextObj.transform.position.z;
		newText.rotX = newTextObj.transform.eulerAngles.x;
		newText.rotY = newTextObj.transform.eulerAngles.y;
		newText.rotZ = newTextObj.transform.eulerAngles.z;
		newText.text = newTextObj.transform.GetComponent<Text> ().text;
		gameDataList.Add (newText);
		++index;
	}

	public void path() {
		filePath.transform.position = menu.transform.position;
		filePath.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		filePath.SetActive (true);
		menu.SetActive (false);
	}

	public void image() {
		filePath.SetActive (false);
		GameObject newImgObj = Instantiate (imgObj, canvas.transform);
		newImgObj.name = "image_"+index;
		newImgObj.transform.position = menu.transform.position;
		newImgObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		newImgObj.transform.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Data/"+filePathObj.text);
		newImgObj.transform.GetChild (0).name = newImgObj.name;
		newImgObj.SetActive (true);
		Serialization.InputJSON newImg = new Serialization.InputJSON ();
		newImg.time = (long) (Player.time * 1000);
		newImg.name = newImgObj.name;
		newImg.type = "Image";
		newImg.posX = newImgObj.transform.position.x;
		newImg.posY = newImgObj.transform.position.y;
		newImg.posZ = newImgObj.transform.position.z;
		newImg.rotX = newImgObj.transform.eulerAngles.x;
		newImg.rotY = newImgObj.transform.eulerAngles.y;
		newImg.rotZ = newImgObj.transform.eulerAngles.z;
		newImg.file = filePathObj.text;
		filePathObj.text = "";
		gameDataList.Add (newImg);
		++index;
	}

	public void editQuestion() {
		questionEditor.transform.position = menu.transform.position;
		questionEditor.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		questionEditor.SetActive (true);
		menu.SetActive (false);
	}

	public void displayQuestion() {
		questionEditor.SetActive (false);
		questionObj.transform.position = menu.transform.position;
		questionObj.transform.LookAt (2*menu.transform.position-m_camera.transform.position);
		question.text = questionInput.text;
		answer1.GetComponentsInChildren<Text>()[0].text = answer1Input.text;
		answer2.GetComponentsInChildren<Text>()[0].text = answer2Input.text;
		answer3.GetComponentsInChildren<Text>()[0].text = answer3Input.text;
		questionObj.SetActive (true);
		Serialization.InputJSON newQuestion = new Serialization.InputJSON ();
		newQuestion.time = (long) (Player.time * 1000);
		newQuestion.type = "Question";
		newQuestion.posX = questionObj.transform.position.x;
		newQuestion.posY = questionObj.transform.position.y;
		newQuestion.posZ = questionObj.transform.position.z;
		newQuestion.rotX = questionObj.transform.eulerAngles.x;
		newQuestion.rotY = questionObj.transform.eulerAngles.y;
		newQuestion.rotZ = questionObj.transform.eulerAngles.z;
		newQuestion.question = questionInput.text;
		newQuestion.answer1 = answer1Input.text;
		newQuestion.answer2 = answer2Input.text;
		newQuestion.answer3 = answer3Input.text;
		questionInput.text = "";
		answer1Input.text = "";
		answer2Input.text = "";
		answer3Input.text = "";
		gameDataList.Add (newQuestion);
		++index;
	}

	public void remove(GameObject button) {
		canvas.transform.Find (button.name).gameObject.SetActive (false);
		Serialization.InputJSON newDelete = new Serialization.InputJSON ();
		newDelete.time = (long) (Player.time * 1000);
		newDelete.type = "Destroy";
		newDelete.name = button.name;
		gameDataList.Add (newDelete);
		++index;
	}

	public void answered() {
		StartCoroutine (wait ());
	}

	IEnumerator wait() {
		yield return new WaitForSeconds (1);
		answer1.interactable = true;
		answer2.interactable = true;
		answer3.interactable = true;
		questionObj.SetActive (false);
	}

	public void Fade(bool show) {
		if (show) {
			StartCoroutine(DoAppear());
		} else {
			StartCoroutine(DoFade());
		}
	}

	IEnumerator DoAppear() {
		CanvasGroup cg = GetComponent<CanvasGroup>();
		while (cg.alpha < 1.0) {
			cg.alpha += Time.deltaTime * 2;
			yield return null;
		}
		cg.interactable = true;
		yield break;
	}

	IEnumerator DoFade() {
		CanvasGroup cg = GetComponent<CanvasGroup>();
		while (cg.alpha > 0) {
			cg.alpha -= Time.deltaTime;
			yield return null;
		}
		cg.interactable = false;
		yield break;
	}

	private string FormatTime(long ms) {
		int sec = ((int)(ms / 1000L));
		int mn = sec / 60;
		sec = sec % 60;
		int hr = mn / 60;
		mn = mn % 60;
		if (hr > 0) {
			return string.Format("{0:00}:{1:00}:{2:00}", hr, mn, sec);
		}
		return string.Format("{0:00}:{1:00}", mn, sec);
	}

	static int SortByTime(Serialization.InputJSON i1, Serialization.InputJSON i2)
	{
		return i1.time.CompareTo(i2.time);
	}



	IEnumerator Upload() {
		gameDataList.Sort (SortByTime);
		gameData.input = gameDataList;

		WWWForm form = new WWWForm ();
		form.AddField ("name", projectName.text);
		form.AddField ("video", videoURL.text);
		form.AddField ("data", JsonUtility.ToJson(gameData).ToString());

		UnityWebRequest www = UnityWebRequest.Post (Constants.Host + "insert.php", form);
		yield return www.Send ();

		if (www.isNetworkError) {
			Debug.Log(www.error);
		}
		else {
			Debug.Log("Form upload complete!");
		}
	}
}
